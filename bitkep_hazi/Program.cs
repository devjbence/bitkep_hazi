﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;

namespace bitkep_hazi
{

    class HeightMap
    {
        static int map_size = 1201;
        int elevationThreshold;
        static byte[] map;
        static HeightMap height_map;
        Bitmap bmp;
        public  int ElevationThreshold
        { get { return elevationThreshold; } set { if (value >= 0 && value <= 500) elevationThreshold = value; } }

        public static HeightMap Parse(string path)
        {
            height_map = new HeightMap();
            FileStream file = new FileStream(path, FileMode.Open);
            using (BinaryReader br = new BinaryReader(file, Encoding.BigEndianUnicode))
            {
                map= new byte[1201 * 1201];
                int i = 0;
                while (i < (map_size * map_size))
                {
                    map[i] = br.ReadByte();
                    i++;
                }
            }
            return height_map;
        }
        private int[] GetExtremalElevations()
        {
            int min = map[0], max = map[0];
            for(int i=0;i<map_size*map_size;i++)
            {
                if(map[i] > max) { max = map[i]; }
                if (map[i] <min ) { min = map[i]; }
            }
            return new int[] { max,min};
        }
        public void SaveToBitmap(string path)
        {
                bmp = new Bitmap(1201, 1201);
                Color c = Color.Blue;
                int i = 0, j = 0, ii = 0;
                while (i < (1201 * 1201))
                {
                    if (map[i] <= elevationThreshold ) { c = Color.FromArgb(0, 0,  map[i]); }
                    else if (map[i] >elevationThreshold) { c = Color.FromArgb(0, map[i], 0); }
                    else if (map[i]== Byte.MaxValue) { c = Color.FromArgb(0, 0, 0); }
                    i++;
                    if (i % 1201 == 0) { ii = 0; }
                    bmp.SetPixel(j, ii, c);
                    if (i % 1201 == 0) { j++; }
                    ii++;
                }
            bmp.Save(path);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //email:kissdanieldezso@gmail.com
            //felhnév: szabo_zs       
            HeightMap h = new HeightMap();
            h= HeightMap.Parse(args[0]);
            h.ElevationThreshold = Convert.ToInt32(args[2]);
            h.SaveToBitmap(args[1]);
            Console.WriteLine("siker");
        }
    }
}
